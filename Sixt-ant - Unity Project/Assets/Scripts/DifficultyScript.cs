﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyScript : MonoBehaviour
{
    public int chosenDifficulty;
    public SceneChanger sceneChanger;
    
    // Start is called before the first frame update
    void Start()
    {
        chosenDifficulty = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Awake()
    {
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public void SetDifficulty(int difficultyButton)
    {
        switch(difficultyButton)
        {
            case 1:
                chosenDifficulty = 1;
            break;

            case 2:
                chosenDifficulty = 2;
            break;

            case 3:
                chosenDifficulty = 3;
            break;
        }

        sceneChanger.ChangeScene("Game");
    }
}
