﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerScript : MonoBehaviour
{

    private FoodScript fScript;
    public QuestionScript qScript;
    public List<Sprite> happyFaces, sadFaces = new List<Sprite>();
    private Text myText;
    public Image myImage;

    // Start is called before the first frame update
    void Start()
    {
        myText = gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
        myImage = gameObject.transform.GetChild(0).gameObject.transform.GetChild(1).gameObject.GetComponent<Image>();
        myImage.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D colObj)
    {
        if(colObj.gameObject.layer == 8)
        {
            colObj.gameObject.transform.SetParent(gameObject.transform);
            colObj.gameObject.layer = 9;

            fScript = colObj.gameObject.GetComponent<FoodScript>();

            if(fScript.isCorrectAnswer == true)
            {
                myImage.sprite = happyFaces[Random.Range(0,happyFaces.Count)];
                myImage.gameObject.SetActive(true);
                fScript.Dissapear();

                qScript.CorrectAnswer();
            }
            else
            {
                myImage.sprite = sadFaces[Random.Range(0,sadFaces.Count)];
                myImage.gameObject.SetActive(true);
                Invoke("Start", 0.5f);
                qScript.WrongAnswer();
                fScript.ResetFood();
                fScript.Dissapear();
            }
        }
    }
}
