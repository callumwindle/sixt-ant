﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonScript : MonoBehaviour, IPointerDownHandler,IPointerUpHandler
{
    public bool upButton, leftButton, rightButton, buttonPressed;
    public PlayerScript psScript;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(buttonPressed)
        {
            if(upButton)
            {
                psScript.destPos = Vector3.Scale(psScript.movePoint.position, new Vector3(1f, 1f, 0f));
                psScript.gameObject.transform.position = Vector3.MoveTowards(psScript.gameObject.transform.position, psScript.destPos ,(Time.deltaTime * psScript.moveSpeed));
            }
            else if(rightButton)
            {
                psScript.gameObject.transform.Rotate( 0f, 0f, (-1f)*psScript.rotateSpeed);
            }
            else if(leftButton)
            {
                psScript.gameObject.transform.Rotate( 0f, 0f, (1f)*psScript.rotateSpeed);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        buttonPressed = true;
        Debug.Log("Down");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        buttonPressed = false;
        Debug.Log("Up");
    }
}
