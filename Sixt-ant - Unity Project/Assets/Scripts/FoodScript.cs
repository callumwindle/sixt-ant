﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FoodScript : MonoBehaviour, IPointerDownHandler
{
    public bool isCorrectAnswer;
    public List<Sprite> foodObjects = new List<Sprite>();
    public int objIndex;
    public Transform foodContainer; 
    public Vector2 curMax, curMin;
    public RectTransform objRect;

    // Start is called before the first frame update
    void Start()
    {
        objRect = gameObject.GetComponent<RectTransform>();
        objIndex = gameObject.transform.GetSiblingIndex();
        PickRandomFood();
    
        curMax = objRect.anchorMax;
        curMin = objRect.anchorMin;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        gameObject.transform.SetParent(foodContainer, true);
        gameObject.transform.SetSiblingIndex(objIndex);

        objRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 0);
        objRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, 0);
        objRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, 0);
        objRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0, 0);

        objRect.anchorMax = curMax;
        objRect.anchorMin = curMin;

        objRect.rotation = Quaternion.identity;

        gameObject.layer = 8;
    }

    void PickRandomFood()
    {
        gameObject.GetComponent<Image>().sprite = foodObjects[Random.Range(0,foodObjects.Count)];
    }

    public void SetCorrectAnswer(bool i)
    {
        isCorrectAnswer = i;
    }

    public void ResetFood()
    {
        gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = "";
        gameObject.transform.SetParent(foodContainer, true);
        gameObject.transform.SetSiblingIndex(objIndex);

        objRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 0);
        objRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, 0);
        objRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, 0);
        objRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0, 0);

        objRect.anchorMax = curMax;
        objRect.anchorMin = curMin;

        objRect.rotation = Quaternion.identity;

        gameObject.layer = 8;

        PickRandomFood();
        Reappear();

        isCorrectAnswer = false;
    }

    public void Dissapear()
    {
        gameObject.SetActive(false);
    }

    public void Reappear()
    {
        gameObject.SetActive(true);
    }
}
