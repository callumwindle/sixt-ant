﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public Transform movePoint;
    public GameObject centrePoint;
    public Vector3 destPos;
    public float rotateSpeed, moveSpeed, speedModifier;
    public float baseRatio, currRatio;
    // Start is called before the first frame update
    void Start()
    {
        rotateSpeed = 2.5f;
        moveSpeed = 200f;
        baseRatio = Mathf.Pow(Mathf.Pow(1920f,2f)+Mathf.Pow(1080f,2f), 0.5f);
        currRatio = Mathf.Pow(Mathf.Pow(Screen.width, 2.0f) + Mathf.Pow(Screen.height, 2.0f),0.5f);

        speedModifier = currRatio/baseRatio;
        moveSpeed *= speedModifier;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetAxis("Horizontal") != 0)
        {
            transform.Rotate( 0f, 0f, ((Input.GetAxis("Horizontal")*-1f)*rotateSpeed));
        }
        
        if(Input.GetAxis("Vertical") == 1)
        {
            destPos = Vector3.Scale(movePoint.position, new Vector3(Input.GetAxis("Vertical"), Input.GetAxis("Vertical"), 0f));
            transform.position = Vector3.MoveTowards(transform.position, destPos,(Time.deltaTime * moveSpeed));
        }
    }

    void FixedUpdate()
    {
        baseRatio = Mathf.Pow(Mathf.Pow(1920f,2f)+Mathf.Pow(1080f,2f), 0.5f);
        currRatio = Mathf.Pow(Mathf.Pow(Screen.width, 2.0f) + Mathf.Pow(Screen.height, 2.0f),0.5f);

        speedModifier = currRatio/baseRatio;

        moveSpeed = 200 * speedModifier;
    }

    private void OnTriggerEnter2D(Collider2D colObj)
    {
        if(gameObject.transform.childCount < 5 && colObj.gameObject.layer == 8) 
        {
            colObj.transform.SetParent(gameObject.transform);
        }
        
    }
}
