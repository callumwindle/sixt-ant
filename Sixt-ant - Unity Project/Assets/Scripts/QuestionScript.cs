﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionScript : MonoBehaviour
{
    public List<GameObject> antSpawn, foodSpawn, screenPoint, spawnMemory = new List<GameObject>();
    public List<string> questionList = new List<string>();
    public GameObject currentAnt, curScreenPoint, endMenu;
    public Text scoreText;
    
    private int curQuestionVal; 
    public int currDiff, finalScore;
    private int qAns, currentAntNum;
    public string currentQuestion;
    private int randomNum, randomAns, currScore;
    private GameObject oriSpawnPoint;
    
    // Start is called before the first frame update
    void Start()
    {
        qAns = 0;
        currScore = 0;
        currDiff = GameObject.Find("DifficultyController").gameObject.GetComponent<DifficultyScript>().chosenDifficulty;
        ResetScore();
        Invoke("NextAnt", 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextAnt()
    {
        randomNum = Random.Range(0, antSpawn.Count);
        
        currentAnt = antSpawn[randomNum].gameObject;
        currentAntNum = randomNum;

        curScreenPoint = screenPoint[randomNum].gameObject; 

        while(Vector3.Distance(currentAnt.transform.position,curScreenPoint.transform.position) >= 0.05f)
        {
            currentAnt.transform.position = Vector3.MoveTowards(currentAnt.transform.position, curScreenPoint.transform.position, Time.deltaTime * 2f);
        }

        NewQuestion();

        
    }
    
    public void NewQuestion()
    {
        randomNum = Random.Range(0, questionList.Count);
        while(randomNum == curQuestionVal)
        {
            randomNum = Random.Range(0, questionList.Count);
        }
        curQuestionVal = randomNum;
        qAns = 6*(randomNum+1);
        currentQuestion = questionList[randomNum] + "?";
        SetQuestion();
        NewAnswers();
    }

    public void SetQuestion()
    {
        currentAnt.transform.GetChild(0).gameObject.transform.GetChild(1).gameObject.SetActive(false);
        currentAnt.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text =  currentQuestion;
    }

    public void NewAnswers()
    {   
        randomNum = Random.Range(0,4);
        
        for(int x = 0; x<foodSpawn.Count;x++)
        {        
            foodSpawn[x].gameObject.SetActive(true);
            foodSpawn[x].gameObject.GetComponent<FoodScript>().SetCorrectAnswer(false);
            if(x != randomNum)
            {
                RandomAnswer();
                foodSpawn[x].gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = randomAns.ToString();
                
            }
            else
            {
                foodSpawn[x].gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = qAns.ToString();
                foodSpawn[x].gameObject.GetComponent<FoodScript>().SetCorrectAnswer(true);
            }
        }
    }

    public void RandomAnswer()
    {
        randomAns = Random.Range(1,72);
        if(randomAns == qAns)
        {
            RandomAnswer();
        }
    }
    
    public void CorrectAnswer()
    {
        currScore++;
        UpdateScore();
        if(currScore != finalScore)
        {
            Invoke("ResetAnt", 1);
        }
    }

    public void WrongAnswer()
    {
        Invoke("SetQuestion", 1);
    }

    public void ResetAnt()
    {
        oriSpawnPoint = spawnMemory[currentAntNum];

        while(Vector3.Distance(currentAnt.transform.position,oriSpawnPoint.transform.position) >= 0.05f)
        {
            currentAnt.transform.position = Vector3.MoveTowards(currentAnt.transform.position,oriSpawnPoint.gameObject.transform.position, Time.deltaTime * 2f);
        }

        currentAnt.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = "?";

        foreach(GameObject x in foodSpawn)
        {
            x.GetComponent<FoodScript>().Dissapear();
            x.GetComponent<FoodScript>().ResetFood();
        }

        Invoke("NextAnt", 0.1f);
    }

    public void ResetScore()
    {
        currScore = 0;
        switch(currDiff)
        {
            case 1:
                finalScore = 3;
            break;

            case 2:
                finalScore = 6;
            break;

            case 3:
                finalScore = 10;
            break;
        }

        scoreText.text = "0/" + finalScore.ToString();
    }

    public void UpdateScore()
    {
        scoreText.text = currScore.ToString() + "/" + finalScore.ToString();

        if(currScore == finalScore)
        {
            endMenu.SetActive(true);
        }
    }
}
